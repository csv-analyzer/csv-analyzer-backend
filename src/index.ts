import express, { Request, Response, Application } from "express";
import bodyParser from "body-parser";
import cors from "cors";
import "dotenv/config";

import { openai } from "./openai/index.js";
import { assisstantsRouter } from "./routes/assistants.js";
import { threadsRouter } from "./routes/threads.js";
import { messagesRouter } from "./routes/messages.js";
import { filesRouter } from "./routes/files.js";

const app: Application = express();
const port = process.env.PORT ?? 8000;

const staticMiddleware = express.static("../files");

// const threadByFile = new Map<number, number>();

app.use(
  cors({
    origin: "http://localhost:5173",
  })
);
app.use(bodyParser.json());
app.use(staticMiddleware);

app.use("/assistants", assisstantsRouter);
app.use("/threads", threadsRouter);
app.use("/messages", messagesRouter);
app.use("/", filesRouter);

app.get("/gpt-test", async (req: Request, res: Response) => {
  try {
    const result = await openai.chat.completions.create({
      model: "gpt-4o",
      messages: [{ role: "user", content: "Say this is a test" }],
      // stream: true,
    });
    res.json(result);
  } catch (e) {
    console.error(e);
    res.status(500).send("Internal Server Error");
  }
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
