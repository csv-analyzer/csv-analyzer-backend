import express from "express";
import { fileId, openai } from "../openai/index.js";

export const threadsRouter = express.Router();

threadsRouter.post("/", async (req, res) => {
  const body = req.body ?? {
    messages: [
      {
        role: "user",
        content:
          "Create 3 data visualizations based on the trends in this file.",
        attachments: [
          {
            file_id: fileId,
            tools: [{ type: "code_interpreter" }],
          },
        ],
      },
    ],
  };
  const thread = await openai.beta.threads.create(body);

  console.log(thread);
  res.json(thread);
});

threadsRouter.get("/:threadId", async (req, res) => {
  const thread = await openai.beta.threads.retrieve(req.params.threadId);
  res.json(thread);
});
