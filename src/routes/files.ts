import express from "express";
import multer from "multer";
import fs from "fs";
import { toFile } from "openai";
import { assisstantId, openai } from "../openai/index.js";
import { csvToJson, getFileContent } from "../utils/files.js";

const filesPath = `${process.cwd()}/files/`;

export const filesRouter = express.Router();
const uploadMiddleware = multer();

filesRouter.get("/csv-file/:threadId", async (req, res) => {
  const thread = await openai.beta.threads.retrieve(req.params.threadId);
  const fileContent = await getFileContent(
    thread.tool_resources?.code_interpreter?.file_ids?.[0]!
  );

  const data = csvToJson(fileContent.toString());

  res.json(data);
});

filesRouter.get("/files/:fileId", async (req, res) => {
  const fileContent = await getFileContent(req.params.fileId);
  res.send(fileContent);
});

filesRouter.post(
  "/upload",
  uploadMiddleware.single("file"),
  async (req, res) => {
    const buffer = req.file?.buffer;
    if (!buffer) return res.status(400).send("no file has been uploaded");

    const file = await openai.files.create({
      file: await toFile(buffer),
      purpose: "assistants",
    });

    fs.writeFileSync(filesPath + file.id, buffer);

    const thread = await openai.beta.threads.create({
      messages: [
        {
          role: "user",
          content:
            "Create as data visualizations as needed based on the trends in this file.",
          attachments: [
            {
              file_id: file.id,
              tools: [{ type: "code_interpreter" }],
            },
          ],
        },
      ],
    });

    const run = await openai.beta.threads.runs.createAndPoll(thread.id, {
      assistant_id: assisstantId,
      instructions:
        "You are great at creating beautiful data visualizations. You analyze data present in .csv files, understand trends, and come up with data visualizations relevant to those trends. You also share a brief text summary of the trends observed.",
    });

    return res.json(thread);
  }
);
