import express from "express";
import { openai } from "../openai/index.js";

export const assisstantsRouter = express.Router();

assisstantsRouter.post("/", async (req, res) => {
  const body = req.body ?? {
    model: "gpt-4o",
    name: "Data visualizer",
    description:
      "You are great at creating beautiful data visualizations. You analyze data present in .csv files, understand trends, and come up with data visualizations relevant to those trends. You also share a brief text summary of the trends observed.",
    instructions:
      "You are great at creating beautiful data visualizations. You analyze data present in .csv files, understand trends, and come up with data visualizations relevant to those trends. You also share a brief text summary of the trends observed.",
    tools: [{ type: "code_interpreter" }],
  };

  const assistant = await openai.beta.assistants.create(body);
  console.log(assistant);
  res.json(assistant);
});

assisstantsRouter.delete("/:assistantId", async (req, res) => {
  await openai.beta.assistants.del(req.params.assistantId);
  res.send("deleted Successfully");
});
