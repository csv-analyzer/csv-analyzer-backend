import express from "express";
import { openai } from "../openai/index.js";

export const messagesRouter = express.Router();

messagesRouter.get("/:threadId", async (req, res) => {
  const messages = await openai.beta.threads.messages.list(req.params.threadId);

  res.json(messages);
});
