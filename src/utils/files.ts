import fs from "fs";
import Papa from "papaparse";
import { openai } from "../openai/index.js";

const filesPath = `${process.cwd()}/files/`;

export function getFileName(path: string) {
  const splits = path.split("/");

  return splits[splits.length - 1];
}

// get the file if exixts or fetch ir from openai and store it
export async function getFileContent(fileId: string) {
  const filePath = filesPath + fileId;
  console.log(filePath);
  let content;

  if (!fs.existsSync(filePath)) {
    const fileInfo = await openai.files.retrieve(fileId);
    const file = await openai.files.content(fileId);
    const fileContent = await file.buffer();
    fs.writeFileSync(`${process.cwd()}/files/${fileInfo.id}`, fileContent);
    content = fileContent;
  } else {
    const fileContent = fs.readFileSync(filePath);
    content = fileContent;
  }

  return content;
}

export function csvToJson(csv: string) {
  const data = Papa.parse<string[]>(csv).data;

  const columns = data[0].map(col => ({ field: col }));
  const rows = data
    .filter(row => row.length === columns.length)
    .slice(1)
    .map(row =>
      row.reduce((acc, curr, idx) => ({ ...acc, [columns[idx].field]: curr }), {
        id: "id" + Math.random().toString(16).slice(2),
      })
    );

  return { rows, columns };
}
