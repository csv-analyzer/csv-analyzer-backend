# CSV Analyzer Backend

This project was built without using a template from ground up.

## Setup

Node Version: `20.9.0`

you can get the server up and running with watch mode using this command

```sh
npm i && npm run dev
```

## Research

After searching through the docs of openai api - which took me a whole day to do so - I found out the following:

- There is an `Assistant API` that accomplishes perfectly the required features.
- An `assistant` can have defined instructions that it can uses to do a certain task.
- You can manage isolated environments aka context using a `thread` for each.
- Each `thread` has its own list of `messages` and `attachments` aka supported files that the assistant can go through to answer some of the chat messages.
- A thread is not processed until a `run` is created which will trigger the whole processing stuff and will generate the user `messages`.
- The `assistant file` aka the csv file has to be stored on the server becasue it cant be downloaded from the openai after that.

## Assumptions

- You have provided your OPENAI_API_KEY to `.env` file.
- There is an assistant created with known id and stored here `src/openai/index.ts`
- Your frontend is running on `http://localhost:5173` for CORS policy.

## DISCLAIMERS

- I have zero experience using the gpt api.
- I did not have the time to do the following:
  - **interactive charts**: since it requires a structured analyzed data instead I rendered the generated images.
  - **user chat**: If I had a couple hours more I would implement it beacause with the help of assistant api it would not take much effort.
- For big csv files I would suggest paginating the rows sent to the client which clearly could not be done in such short period of time.
- To make the status of the processed file gets updated every time the status changes I have to implement a polling mechanism between the server and the client which gladly the openai provides but clearly could not.. .

## Advice

```
"do not lose hope, nor be sad."

Quran 3:139
```
